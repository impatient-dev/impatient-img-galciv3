import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	kotlin("jvm") version "1.3.41"
}

group = "impatient-dev"
version = "0.0.1"

repositories {
	mavenCentral()
}

dependencies {
	implementation(kotlin("stdlib-jdk8"))
	compile("org.apache.logging.log4j:log4j-core:2+")

	compile(project(":impatient-utils"))
	compile(project(":impatient-log4j2"))

	compile("org.apache.commons:commons-io:+")
	compile("org.tomlj:tomlj:1.0+")
}

tasks.withType<KotlinCompile> {
	kotlinOptions.jvmTarget = "1.8"
}