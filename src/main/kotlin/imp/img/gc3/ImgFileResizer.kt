package imp.img.gc3

import imp.util.logger
import java.awt.image.BufferedImage
import java.nio.file.Files
import java.nio.file.Path
import javax.imageio.ImageIO
import kotlin.math.abs


private val TRANSPARENT: Int = 0x00000000

/**It's OK for the aspect ratio to be this much off.*/
private val ASPECT_RATIO_TOLERANCE: Double = 0.1
private fun aspectRatiosSimilar(a: Double, b: Double): Boolean = abs(a - b) < ASPECT_RATIO_TOLERANCE


object ImgFileResizer {

   private val log = ImgFileResizer::class.logger
	/**Converts the input image to PNG and saves it to the output file path (which should end in .png).
	 * If the image's aspect ratio does not match the desired ratio, transparency will be added until it does.
	 * (Note on PNG: JPEG doesn't support transparency.)*/
	fun resizeToRatioPng(input: Path, output: Path, desiredW: Int, desiredH: Int) {
		Files.deleteIfExists(output)
		val img: BufferedImage = ImageIO.read(input.toFile())
		val actualAspectRatio = img.width.toDouble() / img.height
		val desiredAspectRatio = desiredW.toDouble() / desiredH

		val outImg: BufferedImage = if(aspectRatiosSimilar(desiredAspectRatio, actualAspectRatio)) {
			log.debug("\t\t${img.width}x${img.height} already OK")
			img
		} else {
			//calulate new image size (desired < actual aspect ratio means it's currently too wide)
			val outW: Int = if(desiredAspectRatio < actualAspectRatio) img.width else (img.height * desiredAspectRatio).toInt()
			val outH: Int = if(desiredAspectRatio < actualAspectRatio) (img.width / desiredAspectRatio).toInt() else img.height
			log.debug("\t\t${img.width}x${img.height} resizing to ${outW}x$outH")

			//create an image filled with transparency
			val out = BufferedImage(outW, outH, BufferedImage.TYPE_INT_ARGB)
			for(x in 0 until outW)
				for(y in 0 until outH)
					out.setRGB(x, y, TRANSPARENT)

			//copy the original to the center
			val copyStartX = (outW - img.width) / 2
			val copyStartY = (outH - img.height) / 2
			for(x in 0 until img.width)
				for(y in 0 until img.height)
					out.setRGB(copyStartX + x, copyStartY + y, img.getRGB(x, y))
			out
		}

		Files.createDirectories(output.parent)
		if(!ImageIO.write(outImg, "png", output.toFile())) {
			throw Exception("Failed to find an appropriate image writer, on file $output")
		}
	}
}