package imp.img.gc3

import imp.util.logger
import org.tomlj.Toml
import org.tomlj.TomlArray
import org.tomlj.TomlTable
import java.nio.file.Path
import java.nio.file.Paths

class AppConfig (
	val inputDir: Path,
	val outputDir: Path,
	/**A list of valid substitutions, where the 1st in each pair is the "from" type and the 2nd is the "to" type.*/
	substitutions: List<Pair<GC3ImgType, GC3ImgType>>
) {
	/**The key is the "from" type.*/
	private val substitutionsFrom = HashMap<GC3ImgType, HashSet<GC3ImgType>>()
	/**The key is the "to" type.*/
	private val substitutionsTo = HashMap<GC3ImgType, HashSet<GC3ImgType>>()

	init {
		for(pair in substitutions) {
			substitutionsFrom.computeIfAbsent(pair.first, {HashSet()}) .add(pair.second)
			substitutionsTo.computeIfAbsent(pair.second, {HashSet()}) .add(pair.first)
		}
	}

	/**Returns all types that this type can substitute for/to.*/
	fun substitutionsFrom(type: GC3ImgType): Set<GC3ImgType> = substitutionsFrom[type] ?: emptySet()
	/**Returns all types that can be substituted for/to this type (i.e. can cause images of this type to be created).*/
	fun substitutionsTo(type: GC3ImgType): Set<GC3ImgType> = substitutionsTo[type] ?: emptySet()


	fun inputTypeDir(type: GC3ImgType): Path = Paths.get(inputDir.toString(), type.folderName)
	fun outputTypeDir(type: GC3ImgType): Path = Paths.get(outputDir.toString(), type.folderName)
}

private val log = AppConfig::class.logger

fun loadAppConfig(appDir: Path): AppConfig {
	val filePath = Paths.get(appDir.toString(), "gc3ImgConfig.toml")
	log.info("Loading app config from {}", filePath)

	try {
		val data = Toml.parse(filePath)
		val inputDir = requireString(data, "inputDir")
		val outputDir = requireString(data, "outputDir")

		val subs = data["substitute"]
		require(subs == null || subs is TomlArray) {"substitute must be an array"}
		val outSubs = ArrayList<Pair<GC3ImgType, GC3ImgType>>()

		if(subs is TomlArray) {
			for (i in 0 until subs.size()) {
				try {
					val sub = subs[i]
					require(sub is TomlTable) { "Each substitute must be a table (object)." }
					val from = requireImgType(sub, "from")
					val to = requireImgType(sub, "to")
					outSubs.add(Pair(from, to))
				} catch (e: Exception) {
					throw Exception("Problem with substitute #${i + 1}", e)
				}
			}
		}

		return AppConfig(Paths.get(inputDir), Paths.get(outputDir), outSubs)
	} catch(e: Exception) {
		throw Exception("Failed to parse config file $filePath", e)
	}
}


private fun requireString(data: TomlTable, key: String): String {
	val out = data[key]
	if(out == null || out !is String)
		throw Exception("$key must be a string")
	return out
}

private fun requireImgType(data: TomlTable, key: String): GC3ImgType = GC3ImgType.valueOf(requireString(data, key))