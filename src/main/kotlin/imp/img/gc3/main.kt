package imp.img.gc3

import imp.log4j2.ImpLog4j2Init
import imp.util.RootDirFinder
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Paths

private class Main

fun main() {
	//initialize logging (avoid calling any methods that might use logging before this point)
	val appDir = RootDirFinder.find("src/main/kotlin", "impatient-img-galciv3.jar", logging = false)
	val logFile = Paths.get(appDir.toString(), "log4j2.xml")
	ImpLog4j2Init.initWithFile(logFile, "/log4j2-default.xml", Main::class)

	val log = Main::class.logger
	log.info("App starting up; logging initialized.")

	val cfg = loadAppConfig(appDir)
	log.info("Preparing images from {} into {}", cfg.inputDir, cfg.outputDir)
	if(!Files.isDirectory(cfg.inputDir))
		throw Exception("Not a real directory: ${cfg.inputDir}")

	AppProcessing.processImages(cfg)
}

