package imp.img.gc3

import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Collectors


object AppProcessing {
	private val log = AppProcessing::class.logger

	fun processImages(cfg: AppConfig) {
		processDirect(cfg)
		log.info("Done with direct processing; performing desired substitutions.")
		processSubstitutions(cfg)
		log.info("Done with substitutions; renaming files so they're all unique.")
		renameFilesUnique(cfg.outputDir)
	}


	/**Processes input to output; does not substitute types.*/
	private fun processDirect(cfg: AppConfig) {
		for(type in GC3ImgType.values()) {
			val typeDir = cfg.inputTypeDir(type)
			log.debug("Processing type dir {}", typeDir)
			forEachFile(typeDir) {filePath ->
				val relativePathPng = equivalentPngFilename(cfg.inputDir.relativize(filePath))
				val outFilename = addParentsToName(removeTop(relativePathPng))
				val fileOutPath = Paths.get(cfg.outputDir.toString(), type.folderName, outFilename)
				log.debug("Processing: {}  -->  {}", filePath, fileOutPath)
				ImgFileResizer.resizeToRatioPng(filePath, fileOutPath, type.desiredW, type.desiredH)
			}
		}
	}


	private fun processSubstitutions(cfg: AppConfig) {
		for(type in GC3ImgType.values()) {
			val substituteTo = cfg.substitutionsFrom(type)
			if(substituteTo.isEmpty())
				continue
			val typeDir = cfg.inputTypeDir(type)

			forEachFile(typeDir) { filePath ->
				val relativePathPng = equivalentPngFilename(typeDir.relativize(filePath))
				for(toType in substituteTo) {
					val toFilename = addParentsToName(relativePathPng)
					val toPath = Paths.get(cfg.outputTypeDir(toType).toString(), toFilename)
					if(!Files.exists(toPath)) {
						log.debug("Substituting: {}  -->  {}", filePath, toPath)
						ImgFileResizer.resizeToRatioPng(filePath, toPath, toType.desiredW, toType.desiredH)
					}
				}
			}
		}
	}

	/**Converts "foo/bar/baz" into "bar/baz".*/
	private fun removeTop(path: Path, skipIfMissing: Boolean = false): Path {
		require(skipIfMissing || path.nameCount > 1) {"Path too short: $path"}
		if(path.nameCount == 1)
			return path
		return path.subpath(1, path.nameCount)
	}

	/**Converts "foo/bar/baz" into "foo_bar_baz".*/
	private fun addParentsToName(path: Path): String {
		val out = StringBuilder()
		for(i in 0 until path.nameCount) {
			out.append(path.getName(i))
			if(i < path.nameCount - 1)
				out.append('_')
		}
		return out.toString()
	}


	/**Renames files so every file has a unique name, in any subfolder, recursively.*/
	private fun renameFilesUnique(rootDir: Path) {
		renameFilesUniqueHelper(rootDir, HashSet())
	}


	/**Recursive helper.
	 * @param names the set of all names encountered so far; this function will add to the set*/
	private fun renameFilesUniqueHelper(dir: Path, names: MutableSet<String>) {
		//read all files in this dir at once so we don't find any twice (once originally, once after renaming)
		val files = Files.list(dir).collect(Collectors.toList())
		for(file in files) {
			if (Files.isDirectory(file)) {
				renameFilesUniqueHelper(file, names)
			} else {
				val name = file.fileName.toString()
				if(!names.add(name)) {
					//name was already in the set; come up with a different one
					val newName = addNewUniqueName(name, names)
					val newPath = Paths.get(file.parent.toString(), newName)
					log.debug("Renaming {}  -->  {}", file, newName)
					Files.deleteIfExists(newPath)
					Files.move(file, newPath)
				}
			}
		}
	}
}



/**Does nothing if the directory does not exist.*/
private fun forEachFile(dir: Path, block: (filePath: Path) -> Unit) {
	if(Files.isDirectory(dir)) {
		Files.walk(dir).use {stream ->
			stream.filter {Files.isRegularFile(it)} .forEach(block)
		}
	}
}

private fun equivalentPngFilename(path: Path): Path {
	try {
		if (path.endsWith(".png")) return path
		val name = path.fileName.toString()
		val period = name.indexOf('.')
		if (period == -1)
			return path
		val outName = name.substring(0, period) + ".png"
		return if(path.parent == null) Paths.get(outName) else Paths.get(path.parent.toString(), outName)
	} catch(e: Exception) {
		throw Exception("Failed to come up with an equivalent PNG filename for $path", e)
	}
}


private fun addNewUniqueName(origName: String, names: MutableSet<String>): String {
	val period = origName.indexOf('.')
	val prefix = (if(period == -1) origName else origName.substring(0, period)) + "-"
	val suffix = if(period == -1) "" else origName.substring(period)

	for(i in 1 until 20) {
		val out = prefix + Integer.toHexString(i) + suffix
		if(names.add(out))
			return out
	}

	throw Exception("Failed to rename file to be unique (you have too many files with the same name): $origName")
}