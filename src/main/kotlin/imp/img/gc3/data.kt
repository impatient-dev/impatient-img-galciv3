package imp.img.gc3


/**Represents one of the types of faction images Galactic Civilizations 3 can use. Each type gets its own folder.*/
enum class GC3ImgType (
	val desiredW: Int,
	val desiredH: Int
) {
	LeaderBGs(1920, 1080),
	LeaderFGs(1920, 1080),
	Portraits(128, 128),
	Logos(128, 128);

	val folderName = name
	/**Width / height.*/
	val desiredAspectRatio: Double = desiredW.toDouble() / desiredH
}