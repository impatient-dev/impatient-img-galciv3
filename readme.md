This is a script for processing images in order to create custom Galactic Civilizations 3 leaders. Features:

* Converts images to the correct aspect ratios, by adding transparency to the top or sides, as appropriate.
(Portraits are supposed to be a different ratio from LeaderFGs.)
* Duplicates images for use in multiple categories, so you can provide the script a set of Portraits images
and have it generate matching LeaderFGs images, etc.
* Flattens directories (so Portraits/foo/bar.png will be turned into Portraits/foo_bar.png)
* Works with PNG and JPEG files (and perhaps others, but that hasn't been tested).
All images are converted to PNG.

## Usage Steps

1. Create a source directory that will contain the original images (e.g. "gc3src").
2. In the source directory, place images into subdirectories named
**LeaderBGs**, **LeaderFGs**, **Logos**, and/or **Portraits**, as appropriate.
For example, you might have an image located at gc3src/Logos/coolSymbol.png
3. Create a **gc3ImgConfig.toml** configuration file
in the same directory as this application (*not* in the image source directory).
See gc3ImgConfig-default.toml for an example of what a valid configuration looks like.
4. Set the paths in the configuration file to point to your image source directory (e.g. "C:\gc3src")
and some output directory (e.g. "C:\gc3out").
5. Configure any substitutions you want. The default config file will take each Portraits or LeaderFGs file
and turn it into a matching Portraits, LeaderFGs, and Logos file.
6. Run this program. You'll need some familiarity with Java to do this. Use **Java 8**.
7. Your output directory (e.g. "gc3out") should now contain a bunch of resized and/or duplicated images.
Move the LeaderBGs/LeaderFGs/Logos/Portraits directories into **Documents/My Games/GC3Crusade/Factions**.
8. Start up Galactic Civilizations 3. You should now be able to use these images for custom factions.